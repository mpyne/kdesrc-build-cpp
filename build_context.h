#ifndef KSB_BUILD_CONTEXT_H
#define KSB_BUILD_CONTEXT_H

class OptionsMap;

/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <vector>

#include "options_map.h"

using ModuleNameList = std::vector<std::string>;

/**
 * Contains information on the running execution, including:
 *
 * - List of modules to be processed
 * - What build phases to apply for each module
 * - Build phase results
 * - What options have been entered for each module
 *   - Including at various levels of hierarchy
 *     1. Command line
 *     2. Config file (for module, including module-set)
 *     3. Config file (global)
 *     4. Default global settings
 *   - Which rc-files are in effect as a result of above
 * - Log file information
 */
class BuildContext
{
public:
    void assignOptionConfig(OptionsMap buildOpts);
    void assignDefaultOptions(OptionsMap defaultOpts);

    ModuleNameList generateModuleList() const;

    // Returns the resolved value of the requested option for the given module.
    // If the module contains the given option, returns its value.
    // If it doesn't, returns the "global" value instead, or the built-in default value.
    // If no match has been found after this, returns the @p fallback provided in the call.
    std::string getOption(const std::string &module, const std::string &key, const std::string &fallback = "") const noexcept;

private:
    OptionsMap m_defaultOptions;
    OptionsMap m_buildOptions;
};

#endif // KSB_BUILD_CONTEXT_H

/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "build_context.h"

#include <utility>

#include <spdlog/spdlog.h>

using std::vector;
using std::string;

static vector<string> split_on_ws(string in)
{
    vector<string> results;
    auto it = in.cbegin();

    while(it != in.cend()) {
        while(it != in.cend() && *it == ' ') {
            ++it;
        }

        const auto start = it;
        while(it != in.cend() && *it != ' ') {
            ++it;
        }

        if(start != it) {
            results.emplace_back(start, it);
        }
    }

    return results;
}

void BuildContext::assignOptionConfig(OptionsMap buildOpts)
{
    m_buildOptions = std::move(buildOpts);
}

void BuildContext::assignDefaultOptions(OptionsMap defaultOpts)
{
    m_defaultOptions = std::move(defaultOpts);
}

ModuleNameList BuildContext::generateModuleList() const
{
    std::vector<std::string> moduleList;

    for(const auto &[modName, opts] : m_buildOptions.moduleOptionGroups()) {
        const OptionsMap::GroupType modType = m_buildOptions.scopeType(modName);
        switch(modType) {
            case OptionsMap::GroupType::Module:
                moduleList.push_back(modName);
                break;

            case OptionsMap::GroupType::ModuleSet: {
                if(!m_buildOptions.hasOption(modName, "use-modules")) {
                    spdlog::warn("Module set {} is missing a use-modules setting", modName);
                    break;
                }

                const auto &newMods = split_on_ws(m_buildOptions.get(modName, "use-modules"));
                for(const auto &newMod : newMods) {
                    moduleList.emplace_back(modName + "/" + newMod);
                }

                if(m_buildOptions.get(modName, "repository") == "kde-projects") {
                    moduleList.emplace_back(
                            fmt::format("... and modules matching these prefixes for {}", modName));
                }

                // TODO: Need to support ignore-modules

                break;
            }

            case OptionsMap::GroupType::Override:
                // do nothing for now
                break;

            default:
                spdlog::trace("Nothing to generate in module list for {}", modName);
        }
    }

    return moduleList;
}

string BuildContext::getOption(const string &module, const string &key, const string &fallback) const noexcept
{
    return
          m_buildOptions.hasOption  (module  , key) ? m_buildOptions.get  (module  , key)
        : m_buildOptions.hasOption  ("global", key) ? m_buildOptions.get  ("global", key)
        : m_defaultOptions.hasOption("global", key) ? m_defaultOptions.get("global", key)
        : fallback;
}

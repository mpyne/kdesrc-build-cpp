/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <gtest/gtest.h>
#include <fmt/core.h>

#include <filesystem>
#include <tuple>

#include "userconfig.h"
#include "options_map.h"
#include "test_info.h"

const std::filesystem::path testPath(KSB_TEST_DATA_DIR);

TEST(UserConfigTest, Parsing) {
    const static auto data = R"(
# Ignore comment
         global   #####
# Ignore here too
    end global# and ignore here


    )";

    OptionsMap buildOptions;
    readOptionsFromData(buildOptions, data, testPath);

    // Not very useful yet but we should have a single group for 'global'
    // with 0 options
    EXPECT_EQ(buildOptions.size(), 1);

    EXPECT_TRUE(buildOptions.hasOptionGroup("global"));

    const auto &globalMap = buildOptions.moduleOptions("global");

    // Right now there should be no buildOptions in this empty struct
    EXPECT_EQ(globalMap.size(), 0);
}

TEST(UserConfigTest, Continuation) {
    const static auto data = R"(
# Global decl
        global
            git-name      Michael            \
                          Pyne   # comment
            # Comment Again
    end global
    )";

    OptionsMap buildOptions;
    readOptionsFromData(buildOptions, data, testPath);

    // Not very useful yet but we should have a single group for 'global'
    // with 0 options
    EXPECT_EQ(buildOptions.size(), 1);
    EXPECT_TRUE(buildOptions.hasOptionGroup("global"));

    const auto &globalOpts = buildOptions.moduleOptions("global");

    const auto gitOption = globalOpts.find("git-name");
    ASSERT_NE(gitOption, globalOpts.end());

    // Tests line continuation -- leading and trailing whitespace should not be
    // present, and the continuation itself should cause a single space
    // character to be inserted between
    EXPECT_EQ(gitOption->second, "Michael Pyne");
}

TEST(UserConfigTest, FileParse) {
    OptionsMap buildOptions;

    readOptionsFromFile(buildOptions, testPath / "kdesrc-buildrc-test");

    EXPECT_EQ(buildOptions.size(), 6);
    EXPECT_TRUE(buildOptions.hasOptionGroup("global"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("foo"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("blah"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("ksb-juk-opts"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("sub-included"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("final-included"));

    const auto globalOpts = buildOptions.moduleOptions("global");
    EXPECT_EQ(globalOpts.size(), 4);

    const auto fooOpts = buildOptions.moduleOptions("foo");
    EXPECT_EQ(fooOpts.size(), 1);

    const auto blahOpts = buildOptions.moduleOptions("blah");
    EXPECT_EQ(blahOpts.size(), 1);

    const auto ksbJukOpts = buildOptions.moduleOptions("ksb-juk-opts");
    EXPECT_EQ(ksbJukOpts.size(), 1);

    const auto subIncludedOpts = buildOptions.moduleOptions("sub-included");
    EXPECT_EQ(subIncludedOpts.size(), 1);

    const auto finalIncludedOpts = buildOptions.moduleOptions("final-included");
    EXPECT_EQ(finalIncludedOpts.size(), 0);
}

TEST(UserConfigTest, NamelessModuleSet) {
    const static auto data = R"(        # line 1
        global                          # line 2
            # Comment Again             # line 3
    end global                          # line 4
                                        # line 5
    module-set                          # line 6
        # Unnamed module-set
        repository kde-projects
    end module-set

    module foo
        repository git://anongit.kde.org/juk.git
    end module
    )";

    OptionsMap buildOptions;
    readOptionsFromData(buildOptions, data, testPath);

    EXPECT_EQ(buildOptions.size(), 3);
    EXPECT_TRUE(buildOptions.hasOptionGroup("global"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("<unnamed module-set at line 6>"));
    EXPECT_TRUE(buildOptions.hasOptionGroup("foo"));

    EXPECT_THROW(buildOptions.get("juk", "repository"), std::runtime_error);
    EXPECT_EQ(buildOptions.get("<unnamed module-set at line 6>", "repository"), "kde-projects");
    EXPECT_EQ(buildOptions.get("foo", "repository"), "git://anongit.kde.org/juk.git");

    const auto &setOpts = buildOptions.moduleOptions("<unnamed module-set at line 6>");
    EXPECT_EQ(setOpts.size(), 1);
}

/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <stdexcept>

#include <spdlog/spdlog.h>
#include <fmt/core.h>

#include "build_context.h"
#include "default_opts.h"
#include "options_map.h"
#include "userconfig.h"

int main(int argc, char **argv)
{
    spdlog::set_pattern("%v");
    OptionsMap buildOptions;

    try {
        const char *inPath = "t/data/kdesrc-buildrc-test";
        if(argc > 1) {
            inPath = argv[1];
        }

        const std::filesystem::path rc_file(inPath);
        readOptionsFromFile(buildOptions, rc_file);

        for(const auto &[k, v] : buildOptions.moduleOptionGroups()) {
            spdlog::info("Read in options for group '{}' ({} options)", k, v.size());
        }

        BuildContext ctx;
        ctx.assignDefaultOptions(loadDefaultOptions());
        ctx.assignOptionConfig(buildOptions);

        const auto &modList = ctx.generateModuleList();
        for(const auto &mod : modList) {
            fmt::print("Could do something with module {}\n", mod);
        }

        fmt::print("Today's source directory: {}\n", ctx.getOption("global", "source-dir"));

        return EXIT_SUCCESS;
    }
    catch(const char *msg) {
        fmt::print("Exception generated reading config: {}\n", msg);
        return EXIT_FAILURE;
    }
    catch(const std::runtime_error &err) {
        fmt::print("\n***\n\n{}\n***\n", err.what());
    }
    catch(...) {
        fmt::print("Unspecified error received! Exiting\n");
        return EXIT_FAILURE;
    }

    (void) argc;
    (void) argv;
}

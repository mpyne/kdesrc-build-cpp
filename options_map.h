#ifndef KSB_OPTIONS_MAP_H
#define KSB_OPTIONS_MAP_H

/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include <map>
#include <string>

using ModuleOptionMap   = std::map<std::string, std::string>;
using ModGroupOptionMap = std::map<std::string, ModuleOptionMap>;

/**
 * Holds all understood options and can query the appropriate value for
 * each.
 *
 * Includes defaults, command-line options and rc-file.
 *
 * Also holds the user-defined lists of modules, module-sets, and options
 * blocks.
 */
class OptionsMap
{
public:
    enum class GroupType
    {
        Default,
        Cmdline,
        Global,  // also the 'global' module
        Module,
        ModuleSet,
        Override // 'options' blocks, used to update options from a prior decl
    };

    // Used to declare a new option group that can be used later with
    // getOption/setOption
    void declareNewOptionGroup(GroupType type, const std::string &name);

    void set(const std::string &scope, const std::string &optName, std::string value);

    // Completely replaces any existing options with those in @p opts
    void setGroup(const std::string &scope, ModuleOptionMap opts);

    void remove(const std::string &scope, const std::string &optName) noexcept;

    bool hasOptionGroup(const std::string &scope) const noexcept;
    bool hasOption(const std::string &scope, const std::string &optName) const noexcept;

    // Returns a reference to the entire bundle of options for a group, intended
    // to permit range-for on the result.
    const ModGroupOptionMap &moduleOptionGroups() const;

    const ModuleOptionMap &moduleOptions(const std::string &scope) const;

    GroupType scopeType(const std::string &scope) const;

    std::string get(const std::string &scope, const std::string &optName) const;

    std::size_t size() const noexcept;

private:
    ModGroupOptionMap m_userOptions;
    std::map<std::string, GroupType> m_groupTypes;
};

#endif // KSB_OPTIONS_MAP_H

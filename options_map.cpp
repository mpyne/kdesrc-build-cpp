/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "options_map.h"

#include <stdexcept>
#include <utility>

#include <fmt/core.h>

void OptionsMap::declareNewOptionGroup(GroupType type, const std::string &name)
{
    if(m_groupTypes.count(name) > 0) {
        // TODO: Enable this once bug in reading module-sets with no name is fixed.
        return;
//      throw std::runtime_error(fmt::format("Duplicate option group {}", name));
    }

    m_groupTypes[name] = type;
    m_userOptions[name] = { };
}

void OptionsMap::set(const std::string &scope, const std::string &optName, std::string value)
{
    m_userOptions[scope].insert_or_assign(optName, std::move(value));
}

void OptionsMap::setGroup(const std::string &scope, ModuleOptionMap opts)
{
    if(const auto groupIt = m_userOptions.find(scope); groupIt != m_userOptions.end()) {
        swap(groupIt->second, opts);
    } else {
        throw std::runtime_error(fmt::format("Can't mass-load options for unknown group {}", scope));
    }
}

void OptionsMap::remove(const std::string &scope, const std::string &optName) noexcept
{
    if(const auto groupIt = m_userOptions.find(scope); groupIt != m_userOptions.end()) {
        groupIt->second.erase(optName);
    }
}

bool OptionsMap::hasOptionGroup(const std::string &scope) const noexcept
{
    return m_userOptions.find(scope) != m_userOptions.end();
}

bool OptionsMap::hasOption(const std::string &scope, const std::string &optName) const noexcept
{
    if(const auto groupIt = m_userOptions.find(scope); groupIt != m_userOptions.end()) {
        const auto &[k, group] = *groupIt;
        return group.find(optName) != group.end();
    }
    return false;
}

const ModGroupOptionMap &OptionsMap::moduleOptionGroups() const
{
    return m_userOptions;
}

const ModuleOptionMap &OptionsMap::moduleOptions(const std::string &scope) const
{
    const auto &moduleOptsIt = m_userOptions.find(scope);
    if(moduleOptsIt == m_userOptions.end()) {
        throw std::runtime_error(
                fmt::format("Asked for a reference to a non-existant option bundle {}!", scope));
    }

    return moduleOptsIt->second;
}

OptionsMap::GroupType OptionsMap::scopeType(const std::string &scope) const
{
    const auto &groupTypeIt = m_groupTypes.find(scope);
    if(groupTypeIt == m_groupTypes.end()) {
        throw std::runtime_error(fmt::format("Tried to access scopeType of unknown option group {}", scope));
    }

    return groupTypeIt->second;
}

std::string OptionsMap::get(const std::string &scope, const std::string &optName) const
{
    // It's OK to ask for an option value that doesn't exist for a known module (at least for now)
    // but the module itself needs to exist. So if it doesn't -> exception
    const auto &scopeMapIt = m_userOptions.find(scope);
    if(scopeMapIt == m_userOptions.end()) {
        throw std::runtime_error(fmt::format("Tried to read '{}' of unknown module group {}", optName, scope));
    }

    const auto &[k, moduleOpts] = *scopeMapIt; // bind to result
    const auto &modOptsIt = moduleOpts.find(optName);
    if(modOptsIt == moduleOpts.end()) {
        return {};
    }

    return modOptsIt->second;
}

std::size_t OptionsMap::size() const noexcept
{
    return m_userOptions.size();
}

/**
 * Copyright 2021 Michael Pyne <mpyne@kde.org>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

#include "userconfig.h"

#include <fmt/core.h>
#include <fmt/color.h>

#include <lexertl/debug.hpp>
#include <lexertl/generator.hpp>
#include <lexertl/lookup.hpp>

#include <cerrno>
#include <filesystem>
#include <fstream>
#include <ios>
#include <stdexcept>
#include <system_error>
#include <vector>

#include "options_map.h"

// Uncomment if you need to use the lexertl debugger
// #include <iostream>

namespace fs = std::filesystem;

// internal detail only, hide from other compilation units
namespace {

    enum LexerStates {
        eEndOfInput   = 0, // lexertl will output state 0 on EOF
        eGlobalDecl   = 1,
        eOptName      = 2,
        eOptValue     = 3,
        eEndOptBlock  = 4,
        eModBlockDecl = 5,
        eModBlockName = 6,
        eNewline      = 7,
        eWhitespace   = 8,
        eComment      = 9,
        eIncludeDecl  = 10,
        eIncludeName  = 11,
        eLineContinue = 12,
        eUnknown      = 65535,  // should be lexertl's npos() value
    };

}

static lexertl::state_machine buildLexerStateMachine()
{
    lexertl::rules rules;
    lexertl::state_machine sm;

    // The lexer can support simple multi-state machine handling. The initial
    // state is built-in and called "INITIAL", which we use for the top-level
    // scanning outside of module/module-set/options blocks.
    rules.push_state("TOPLEVEL");        // State 1
    rules.push_state("MOD_BLOCK_NAME");  // State 2
    rules.push_state("OPT_NAME");        // State 3
    rules.push_state("OPT_WS");          // State 4
    rules.push_state("OPT_VALUE");       // State 5
    rules.push_state("INIT_INC_NAME");   // State 6
    rules.push_state("TOPL_INC_NAME");   // State 7
    rules.push_state("OPTIONAL_NAME");   // State 8

    // Each rule builds into a table of: [ lexer state, regex to match, id to emit, state to transition ]
    // (the ID emission / state transition obviously only occurs if the regex matches)

    // Initially we expect a 'global' section. After that section is read, all
    // subsequent sections should be 'module', 'module-set' or 'options', which is
    // handled by the 'TOPLEVEL' lexer state.
    rules.push("INITIAL", "global", eGlobalDecl, "OPT_NAME");

    rules.push("INITIAL",  "include", eIncludeDecl, "INIT_INC_NAME");
    rules.push("TOPLEVEL", "include", eIncludeDecl, "TOPL_INC_NAME");

    rules.push("INIT_INC_NAME", R"([^#\s]+)", eIncludeName, "INITIAL");
    rules.push("TOPL_INC_NAME", R"([^#\s]+)", eIncludeName, "TOPLEVEL");

    // Check for ending the option block before looking for option values
    rules.push("OPT_NAME",  R"(end +[\w-]+)", eEndOptBlock, "TOPLEVEL");

    // Option name is any "word" chars
    rules.push("OPT_NAME",  R"([a-zA-Z0-9_-]+)", eOptName,  "OPT_WS");

    // Ignore some mandatory whitespace
    rules.push("OPT_WS", "[ \\t]+", eWhitespace, "OPT_VALUE");

    // If there's a line continuation character, move to the next line
    rules.push("OPT_VALUE", R"(\s*\\\n)", eLineContinue, "OPT_VALUE");

    // If not, look for a quoted value if present
    rules.push("OPT_VALUE", R"(["][^"]*["])", eOptValue, "OPT_VALUE");

    // If not a quoted value, non-greedy capture non-comment portion as the value
    // We may end up with some trailing whitespace so we can avoid picking up
    // the newline, lexertl has no clean way to do lookahead assertions.
    rules.push("OPT_VALUE", R"([^#\n\s]+)", eOptValue, "OPT_VALUE");

    rules.push("TOPLEVEL", "options|module", eModBlockDecl, "MOD_BLOCK_NAME");
    rules.push("TOPLEVEL", "module-?set", eModBlockDecl, "OPTIONAL_NAME");
    rules.push("OPTIONAL_NAME,MOD_BLOCK_NAME", "[a-zA-Z0-9_-]+", eModBlockName, "OPT_NAME");

    // '*' as the state applies the rule to all states. Use this for common rules.
    // '.' as the end state means not to switch the current state at all.
    rules.push("*", "\\n", eNewline, "."); // Count lines separately
    rules.push("*", "[ \\t]+", eWhitespace, "."); // Skip non-syntatical whitespace
    rules.push("*", R"(#.*$)", eComment, "."); // Ignore commented portions of lines

    lexertl::generator::build(rules, sm);

    return sm;
}

static std::string readFileContents(const fs::path &in_file)
{
    const auto fsize = fs::file_size(in_file);

    std::ifstream rcFile(in_file);
    std::string contents(fsize, '\0');

    if(rcFile.read(&contents[0], (fsize & 0x00FFFFFF))) {
        return contents;
    }

    throw std::runtime_error(std::string("Unable to read file! ") + in_file.native());
}

void readOptionsFromData(OptionsMap &options, const std::string &in, const fs::path &base)
{
    fmt::print("Reading config file relative to {}\n", fs::canonical(base).native());

    lexertl::state_machine sm{buildLexerStateMachine()};

    // We will have a stack of lexers, one per input rc-file (each rc-file may
    // use the 'include' command to read-in text from separate files... we handle
    // this by dropping in a new lexer at that point until input is consumed).
    //
    // This is the data we need for each lexer
    struct LexerStackEntry {
        std::string     input_text;
        fs::path        path;
        lexertl::smatch lexer;
        int             curLine = 1;

        LexerStackEntry(std::string input, fs::path _path)
            : input_text(std::move(input))
            , path (std::move(_path))
            , lexer(input_text.cbegin(), input_text.cend())
        {
            if(fs::is_directory(path)) {
                // We don't need a file but we need parent_path called later to have something to strip off
                path = path / ".";
            }
        }
    };

    std::vector<LexerStackEntry> lexerStack;
    lexerStack.emplace_back(in, base);

    std::string curGroupName;
    std::string curGroupType;
    std::string curOption;
    typename decltype(sm)::id_type lastLexToken = 0;

#if 0
#define KSB_TRACE(...) fmt::print(__VA_ARGS__)
#else
#define KSB_TRACE(...) (void)"";
#endif

    do {
        auto &curLexEntry = lexerStack.back();
        auto &curLexer = curLexEntry.lexer;
        lexertl::lookup(sm, curLexer);

        KSB_TRACE("l{:03d} {:02d}/{:02d}: ", curLexEntry.curLine, curLexer.id, curLexer.state);
        LexerStates curState = static_cast<LexerStates>(curLexer.id);
        switch(curState) {

            case eGlobalDecl:
                curGroupName = std::string("global");
                options.declareNewOptionGroup(OptionsMap::GroupType::Global, curGroupName);
                KSB_TRACE("Opening GLOBAL options group\n");
                break;

            case eModBlockDecl:
                curGroupType = curLexer.str();
                curGroupName.clear();
                KSB_TRACE("Encountered new options group of type {}\n", curGroupType);
                break;

            case eEndOptBlock:
                KSB_TRACE("Ended current options group '{} {}'\n", curGroupType, curGroupName);
                break;

            case eModBlockName:
                curGroupName = curLexer.str();
                OptionsMap::GroupType type;

                if(curGroupType == "module-set") {
                    type = OptionsMap::GroupType::ModuleSet;
                }
                else if(curGroupType == "options") {
                    type = OptionsMap::GroupType::Override;
                }
                else if(curGroupType == "module") {
                    type = OptionsMap::GroupType::Module;
                }
                else {
                    throw std::runtime_error("Unreachable execution point reached reading option group!");
                }

                options.declareNewOptionGroup(type, curGroupName);
                KSB_TRACE("Opening {} block named {}\n", curGroupType, curGroupName);
                break;

            case eOptName: {
                curOption = curLexer.str();
                KSB_TRACE("Reading option named {}\n", curOption);
                if(options.hasOption(curGroupName, curOption)) {
                    KSB_TRACE("\tOption {} was already present, erasing\n", curOption);
                    options.remove(curGroupName, curOption);
                }
                break;
            }

            case eOptValue: {
                const auto &optValue = curLexer.str();

                if(options.hasOption(curGroupName, curOption)) {
                    const auto newValue = options.get(curGroupName, curOption) + " " + optValue;
                    options.set(curGroupName, curOption, newValue);
                    KSB_TRACE("Updated option value for {} to {}\n", curOption, newValue);
                }
                else {
                    options.set(curGroupName, curOption, optValue);
                    KSB_TRACE("Reading option value {}\n", optValue);
                }

                break;
            }

            case eLineContinue:
                KSB_TRACE("Found line continuation character, skipping.\n");
                curLexEntry.curLine++;
                break;

            case eEndOfInput:
                lexerStack.pop_back();
                KSB_TRACE("End of input\n", curLexer.str());
                break;

            case eWhitespace:
                KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                        "Encountered whitespace.\n");
                break;

            case eNewline:
                // We stay in 'OPT_VALUE' state (5) until the newline is read
                // so we can piece option values together, except for line
                // continuation. Once we see the newline, manually advance the
                // lexer to the right state
                if(lastLexToken != eLineContinue && curLexer.state == 5) {
                    KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                            "Encountered a newline, advanced to OPT_NAME state.\n");
                    curLexer.state = 3; // OPT_NAME
                }
                else if(curLexer.state == 8 /* OPTIONAL_NAME */ && curGroupName.empty()) {
                    // unnamed module-set, generate a name to make it unique
                    KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                            "Encountered a newline for unnamed module-set.\n");

                    curGroupName = fmt::format("<unnamed module-set at line {}>", curLexEntry.curLine);
                    options.declareNewOptionGroup(OptionsMap::GroupType::ModuleSet, curGroupName);
                    curLexer.state = 3; // OPT_NAME
                }
                else {
                    KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                            "Encountered a newline.\n");
                }

                curLexEntry.curLine++;

                break;

            case eComment:
                KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                        "Encountered a comment {}\n", curLexer.str());
                break;

            case eIncludeDecl:
                KSB_TRACE(fmt::emphasis::italic | fg(fmt::color::gray),
                        "Encountered include declaration\n", curLexer.str());
                break;

            case eIncludeName: {
                const auto parent_path = curLexEntry.path.parent_path();
                KSB_TRACE("Including file {} recursively from base path {}.\n",
                        curLexer.str(), parent_path.native());
                const fs::path newFilePath = parent_path / fs::path(curLexer.str());

                KSB_TRACE("\tThis resolves to {}\n", fs::canonical(newFilePath).native());
                const auto newFileContents = readFileContents(newFilePath);

                // The existing lexer might have already reached a non-INITIAL
                // state, but the new one always starts in INITIAL. Need to
                // manually place it in the correct state for it to pick up
                // correctly.
                const auto &oldState = curLexer.state;

                lexerStack.emplace_back(
                        newFileContents,
                        fs::canonical(newFilePath));
                lexerStack.back().lexer.state = oldState;

                break;
            }

            case eUnknown:
                std::string::const_iterator it = curLexer.first;
                while(it != curLexEntry.input_text.end() && *it != '\n' && (it - curLexer.first) < 20) {
                    ++it;
                }

                std::string message = fmt::format("Unable to read config file {} at line {}!\nStarting at: --> {}\n",
                        curLexEntry.path.native(), curLexEntry.curLine, std::string(curLexer.first, it));

                throw std::runtime_error(message);
                break;

            // default not handled to elicit compiler warnings if we forget a case
        }

        lastLexToken = curLexer.state;

    } while(!lexerStack.empty());
}

void readOptionsFromFile(OptionsMap &options, const fs::path &in_file)
{
    return readOptionsFromData(options, readFileContents(in_file), in_file);
}

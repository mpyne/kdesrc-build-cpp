# Overview

This is an attempt to write a C++ version of kdesrc-build to enable:

* Support for hosting a DBus interface or server API permitting simple
  introspection during a build
* Support for languages that are better known by the KDE community
* Support for easier, more modern deployment models, such as a static-built
  executable that can be simply downloaded.
* Retain the benefit of things like parallel download / build

# Build

This is just a shell example at this point (literally it does nothign but echo
its command line arguments)

To build it, first configure with CMake (this example assumes you have Ninja,
which you should if you're doing KDE development):

    $ cmake -G Ninja -S . -B build

(this assumes you have a terminal open to the repository directory and want to
make the build directory named `build` under the directory).

Once configured, you can build the executable using CMake as well:

    $ cmake --build build

## CMake Configure Options

The following options can be passed to `cmake` during configure to change how
the build system is generated:

* `KSB_DISABLE_TESTS`: Set to `ON` to prevent the build options for running
the test suite from being enabled, which saves a dependency on
[Google Test](https://github.com/google/googletest). Defaults to `OFF`.

## Build Targets

When cmake has run, the following build targets can be used (use as
`cmake --build ${build_dir} --target ${TARGET_AS_BELOW} -j $(nproc)`).

* `install`, builds and installs, maintaining debugging symbols and metadata
* `install/strip`, builds and installs and strips symbols from the executable
* `test` (if `KSB_DISABLE_TESTS` is not present), runs the test suite using CTest.

# Run

Once built, you can run the executable directly from the build directory:

    $ ./build/ksb-ng

Or you can install it first and run it from its install path.
